\documentclass[a4paper]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}

% \usepackage[ngerman]{babel}

\usepackage{amsmath}						%Mathematikpakete
\usepackage{amssymb}
\usepackage{amsthm}

\usepackage[]{graphicx}					%Graphikpaket



\DeclareMathOperator{\Const}{Const} 		%Festlegung von Funktionen
\DeclareMathOperator{\ZFC}{ZFC}
\DeclareMathOperator{\bij}{bijektiv}
\DeclareMathOperator{\Dom}{Dom}
\DeclareMathOperator{\Ran}{Ran}
\DeclareMathOperator{\Fund}{Fund}

\newtheorem{thm}{Theorem}				%Festlegungen von Bezeichnungen
\newtheorem{axiom}{Axiom}
\newtheorem{kor}{Korollar}
\newtheorem{defn}{Definition}

\title{Grundlagen der Mengenlehre}
\author{Michael Janny}
\date{ }

\begin{document}

\vspace{0.3cm}

\section*{Was sind Mengen?}

Der Begründer der Mengenlehre Georg Cantor beschrieb Mengen 1895 auf folgende Weise:

\begin{quote}
  Unter einer "Menge" verstehen wir jede Zusammenfassung $M$ von bestimmten wohlunterschiedenen Objekten $m$ unserer Anschauung oder unseres Denkens (welche die "Elemente" von $M$ genannt werden) zu einem Ganzen.
\end{quote}


Ein weiterer Pionier der modernen Mathematikphilosophie Gottlob Frege versuchte den Mengenbegriff durch das Extensionalitätsaxiom (siehe unten) und das (naive) Komprehensionsprinzip zu fassen. Letzteres geht davon aus, dass man zu jeder Eigenschaft $\varphi$ die "Menge" aller Objekte, die die Eigenschaft $\varphi$ haben, bilden könne. Schreibt man für die Behauptung, dass $x$ die Eigenschaft $\varphi$ hat, kurz $\varphi (x)$, dann könnte man diese "Menge" $M$ auch in folgender Form angeben:

\[  M=\{ x : \varphi (x) \} \]

Bertrand Russell wies aber 1903 nach, dass das naive Komprehensionsprinzip zu Widersprüchen führt. Er zeigte, dass es nicht zu jeder Eigenschaft $\varphi$ eine Menge geben könne. Nimmt man nämlich als Eigenschaft $\varphi$ die Eigenschaft, kein Element von sich selbst zu sein ($ \varphi (x) = x \notin x$), so ergibt sich der folgende Widerspruch:

Sei $R = \{ x : x \notin x \}$, so kann $R$ weder Element von sich selbst, noch kein Element von sich selbst sein (Widerspruch!). Sei nämlich einerseits $R \in R$, dann hat $R$ die Eigenschaft kein Element von sich selbst zu sein und somit gilt: $R \notin R$. Sei aber andererseits $R \notin R$, dann muss nach Definition von $R$ gelten: $R \in R$. Symbolisch kann man dies auch kurz so schreiben:

\[ R \in R \leftrightarrow R \notin R \]

Um diesen Widerspruch zu umgehen, hat Ernst Zermelo 1908 vorgeschlagen, für die Mengenlehre ein Axiomensystem zu entwickeln, in welchem - zumindest vorderhand - solche Probleme nicht mehr auftreten können. 1921 hat Abraham Fraenkel ein weiteres Axiom hinzugefügt, weshalb man heute von der Zermelo-Fraenkel-Mengenlehre spricht, welche im weiteren dargestellt sei. 

\section*{Zermelo-Fraenkel-Mengenlehre}

Wir bezeichnen dieses Axiomensystem kurz mit $\ZFC$, dabei steht Z für Zermelo, F für Fraenkel und C für "Choice". (Letzteres bezieht sich auf eines der Axiome, nämlich das Auswahlaxiom, welches im Laufe der Zeit nicht unumstritten blieb, welches aber in der Mathematik üblicherweise anerkannt wird.)

\begin{axiom} [Existenzaxiom]
  Es gibt eine Menge.

  Symbolisch: $\exists x (x=x)$

\end{axiom}

Das folgende Axiom legt die Mengenlehre auf den so genannten extensionalen Standpunkt fest, d.h. eine Menge ist eindeutig durch ihre Elemente bestimmt. Es kann also nicht zwei verschiedene Mengen geben, die genau die gleichen Mengen enthalten.

\begin{axiom} [Extensionalitätsaxiom]
  Enthalten zwei Mengen genau die gleichen Elemente, so sind die beiden Mengen identisch.

  Symbolisch: $\forall z (z \in x \leftrightarrow z \in y) \rightarrow x=y$

\end{axiom}

Die Menge der aktuellen amerikanischen Präsidenten und die Menge, der Männer mit hässlichem Toupet, könnten also identisch sein.

Wenn $x$ und $y$ Mengen sind, so macht es Sinn, auch $\{ x,y \}$ als Menge anzusehen. Deshalb sollte man das folgende Axiom akzeptieren:

\begin{axiom} [Paarmengenaxiom]
  Sind $x$ und $y$ Mengen, so gibt es auch eine Menge, welche die beiden Mengen $x$ und $y$ enthält.

  Symbolisch: $\exists z (x \in z \land y \in z)$

\end{axiom}

Daraus folgt leider noch nicht, dass es die Menge $\{ x,y \}$ gibt. Es folgt lediglich, dass es eine Menge gibt, die mindestens die beiden Elemente $x$ und $y$ enthält, sie könnte aber auch noch andere Elemente enthalten. Mit dem folgenden Axiom kann man dieses Problem beheben:

\begin{axiom} [Aussonderungsaxiom]

  \emph{Für jede Menge $v$ und jede Eigenschaft $\varphi$, die sich nicht auf die Menge $y$ bezieht, gilt:}

  Es gibt eine Menge $y$, die genau die Elemente von $v$ enthält, die die Eigenschaft $\varphi$ haben.

  Symbolisch: $\exists y \forall x ( x \in y \leftrightarrow  x \in v \land \varphi (x))$

\end{axiom}

Für die Menge, die durch das Aussonderungsaxiom entsteht, schreiben wir auch:
\[ \{x \in v : \varphi (x) \} \]


Es lässt sich nun die Existenz der oben angegebenen Paarmenge begründen: Seien $x$ und $y$ Mengen, so gibt es nach dem Paarmengenaxiom eine Menge $z$, die die beiden Elemente enthält (evtl. enthält sie aber auch noch weitere Elemente). Mit dem Aussonderungsaxiom können wir nun aber auf die Existenz der Menge $\{ x,y \} := \{ w \in z : w=x \lor w=y \}$ schließen. Mit dem Extensionalitätsaxiom kann man sogar auf die Eindeutigkeit dieser Menge $\{ x,y \}$ schließen, dies rechtfertigt streng genommen erst, dass wir ihr einen eigenen Namen, nämlich $\{ x,y \}$, geben (bei der Einführung weiterer Operationen und Konstanten werden wir den Hinweis auf die Notwendigkeit, das Extensionalitätsaxiom zu verwenden, der Kürze halber nicht mehr erwähnen). Mit $\{ x \} := \{ x,x \}$ ist nun auch die Existenz und Eindeutigkeit von Mengen mit nur einem Element zu rechtfertigen.

Auch die Existenz und Eindeutigkeit (siehe den Hinweis im letzten Absatz) der leeren Menge $\emptyset$ lässt sich nun begründen (in der Mengenlehre ist die aus der Schule bekannte Bezeichnung $\{ \}$ nicht üblich): Sei $z$ irgendeine Menge (die aufgrund des Existenzaxioms existiert), dann existiert aufgrund des Aussonderungsaxioms auch die Menge $\emptyset := \{ x \in z : x \neq x \}$. 

Vergleicht man das Aussonderungsaxiom mit Freges naivem Komprehensionsaxiom, erkennt man, dass ersteres gegenüber zweiterem eine eingeschränkte Aussage trifft. In die neugebildete Menge werden nur die Mengen mit Eigenschaft $\varphi$ aufgenommen, die auch Elemente der Menge $v$ sind. Damit ist der Widerspruch von Russell aus $\ZFC$ nicht ableitbar (falls $\ZFC$ widerspruchsfrei ist). Man kann aber eine ähnliche Argumentation heranziehen, um in $\ZFC$ zu beweisen, dass es keine Allmenge gibt:

\begin{thm}
  Es gibt keine Menge, die alle Mengen enthält.
\end{thm}

\begin{proof} [Beweis]
  Wir nehmen indirekt an, dass es eine Allmenge $V$ doch gibt und führen dies zu einem Widerspruch:
  
  Existiert die Allmenge $V$, so können wir mit dem Aussonderungsaxiom auch die Russellmenge $R := \{x \in V : x \notin x \}$ bilden. Für diese Menge $R$ gilt nun aber wiederum der Widerspruch:
  \[ R \in R \leftrightarrow R\notin R \]
  
  Da somit $R$ nicht existieren kann, kann auch die Menge $V$ nicht existieren.
  
\end{proof}

Wir identifizieren die leere Menge $\emptyset$ mit der kleinsten natürlichen Zahl $0$. Mittels Paarbildungs- und Aussonderungsaxiom können wir nun auch die Existenz der folgenden Mengen bzw. natürlichen Zahlen rechtfertigen:
\[ 1 := \{ \emptyset \} \]
\[ 2 := \{ \emptyset, \{ \emptyset \} \} \]


Man kann aber mit den bisherigen Axiomen nicht gewährleisten, dass es Mengen mit mehr als zwei Elementen gibt. Dazu benötigt man ein weiteres Axiom. Ein möglicher Kandidat für ein solches Axiom wäre: Wenn $x$ und $y$ Mengen sind, dann existiert auch eine Menge $z$, die alle Elemente von $x$ und alle Elemente von $y$ enthält.

Nachdem man auch die Menge $\{ 2 \}$ gebildet hat, kann man nun mit obigem Axiom eine Menge bilden, die die Elemente von $2$ und auch das einzige Element von $\{ 2 \}$
enthält. Mit dem Aussonderungsaxiom gibt es sogar eine Menge, die genau diese Elemente und sonst keine weiteren enthält. Somit haben wir eine mengentheoretische Darstellung der Zahl $3$ gefunden:
 
\[ 3 := \{ 0,1,2 \} \]

Auf die gleiche Weise könnte man nun auch alle weiteren natürlichen Zahlen bilden.

Wir verallgemeinern jedoch das obige Axiom und verwenden im Weiteren:

\begin{axiom} [Vereinigungsaxiom]
  Ist $F$ eine Menge, so gibt es eine Menge $A$, welche alle Elemente $x$ aller Elemente $Y$ von $F$ enthält.
  
  Symbolisch: $\exists A \forall Y \forall x (x \in Y \land Y \in F \rightarrow x \in A)$
\end{axiom}

Wir führen nun mehrere mengentheoretische Schreibweisen und Operationen ein.

Für Mengen $x$ und $y$ definieren wir die übliche Teilmengenbeziehung:

\[ x \subseteq y \Leftrightarrow \forall z (z \in x \rightarrow z \in y) \]

Die Vereinigung von $x$ und $y$ existiert aufgrund des Vereinigungs- und des Aussonderungsaxioms:
 
\[ x \cup y := \{z : \exists Y (Y \in \{ x,y \} \land z \in Y \} \]

Für den Durchschnitt und die Differenz von $x$ und $y$ benötigt man nur das Aussonderungsaxiom:

\[ x \cap y := \{z \in x : z \in y \} \]
\[ x \setminus y := \{z \in x : z \notin y \} \]

Die Nachfolgeroperation

\[ S(x) := x \cup \{ x \} \]

bildet zu jeder natürlichen Zahl $x$ ihren Nachfolger $x+1$:

\[ 4 := S(3) = 3 \cup \{ 3 \} = \{ 1,2,3 \} \cup \{ 3 \} = \{ 0,1,2,3 \} \]

Damit lässt sich auch ein Axiom angeben, welches erstmals die Existenz einer unendlichen Menge (wenn wir auch den Begriff der Unendlichkeit hier nicht exakt definieren, was man aber im Rahmen von $\ZFC$ sehr wohl könnte) garantiert:

\begin{axiom} [Unendlichkeitsaxiom]
  Es gibt eine Menge $x$, für die gilt (1) $\emptyset \in x$ und (2) wenn $y \in x$, dann auch $S(y) \in x$.
  
  Symbolisch: $\exists x (\emptyset \in x \land \forall y (y \in x \rightarrow S(y) \in x))$
\end{axiom}

Eine Menge $x$, welche die Eigenschaft $\emptyset \in x \land \forall y (y \in x \rightarrow S(y) \in x)$ erfüllt, bezeichnet man als induktiv (für die Behauptung, dass $x$ induktiv ist, schreiben wir kurz $induktiv(x)$). Mit dem Unendlichkeits- und dem Aussonderungsaxiom kann man die Menge der natürlichen Zahlen rechtfertigen:
\[ \mathbb{N} := \{z : \forall I (induktiv(I) \rightarrow z \in I) \} \]

D.h. die Menge $\mathbb{N}$ enthält genau die Elemente $z$, welche in allen induktiven Mengen enthalten sind. Dies sind gerade die natürlichen Zahlen ($0,1,2,3,...$).

Um noch weitere Mengen zu erhalten, wird das folgende Axiom zu $\ZFC$ hinzugefügt:

\begin{axiom} [Potenzmengenaxiom]
  Zu jeder Menge $x$ gibt es eine Menge $y$, welche alle Teilmengen von $x$ enthält.
  
  Symbolisch: $\exists y \forall z ( z \subseteq x \rightarrow z \in y)$ 
\end{axiom}

Mit dem Potenzmengen- und Aussonderungsaxiom lässt sich nun die Potenzmenge von $x$ definieren, d.h. die Menge aller Teilmengen von $x$:
\[ Pot(x) := \{ z : z \subseteq x \} \]

Aufgrund einer genialen Idee von Kazimierz Kuratowski kann man auch geordnete Paare, welche in der Mathematik häufig gebraucht werden, als Mengen definieren:
\[ (x,y) := \{ \{ x \}, \{ x,y \} \} \]

Das folgende Theorem zeigt, dass sich geordnete Paare à la Kuratowski ebenso verhalten, wie man es von ihnen erwartet (z.B. gilt $(1,2) \neq (2,1)$):

\begin{thm} 
  Es gilt für alle Mengen $x,y,u,v$:
  \[ (x,y) = (u,v) \Leftrightarrow x=u \land y=v \]
\end{thm}

\begin{proof} [Beweis]
  Wir unterscheiden zwei Fälle:
  
  Im ersten Fall gelte $x=y$. Dann ist $\{ x \} = \{ x,y \} = \{ u \} = \{ u,v \}$, also $x=u$ und $y=v$.
  
  Im zweiten Fall gelte $x \neq y$. Dann ist $\{ x \} = \{u \}$ und $\{ x,y \} = \{ u,v \}$, also $x=u$ und $y=v$.   
\end{proof}

Wir definieren nun das kartesische Produkt der Mengen $A$ und $B$:
\[ A \times B := \{ (x,y) \in Pot(Pot(A \cup B)) : x \in A \land y \in B \} \]

Die Existenz dieser Menge ist durch das Aussonderungs- und Potenzmengenaxiom gesichert. In $A \times B$ sollen alle Paare $(x,y)$ gesammelt werden, für die $x \in A$ und $y \in B$ gelten. Dass diese Paare allesamt in $Pot(Pot(A \cup B))$ enthalten sind, ergibt sich daraus, dass $\{ x \}, \{ x,y \} \in Pot(A \cup B)$ und somit $(x,y) = \{ \{ x \}, \{ x,y \} \} \in Pot(Pot(A \cup B))$.

Wir wollen nun auch Relationen (Zuordnungen) und Funktionen definieren. Diese Definitionen sollen mit den üblichen Definitionen in der Mathematik übereinstimmen.

\begin{defn}
  $R$ ist genau dann eine Relation über $A \times B$, wenn $R \subseteq A \times B$. 
\end{defn}

Die Kleinerrelation $<$ stellt beispielsweise eine Relation über $\mathbb{N} \times \mathbb{N}$ dar:
\[ < := \{ (x,y) \in \mathbb{N} \times \mathbb{N} : x \in y \} \]

Dabei wird genutzt, dass in unserer mengentheoretischen Definition der natürlichen Zahlen, eine solche stets die Menge aller kleineren natürlichen Zahlen ist (z.B. $100$ ist die Menge der Zahlen $0$, $1$, ..., $99$) und somit kleinere Zahlen Elemente größerer Zahlen sind. Es gilt nun z.B. $(1,2) \in <$, wofür wir wie üblich kürzer $1<2$ schreiben.

\pagebreak

\begin{defn}
  $f$ ist genau dann eine Funktion von $A$ nach $B$ (in Zeichen: $f: A \rightarrow B$), wenn die folgenden beiden Bedingungen erfüllt sind:
  \begin{enumerate}
    \item f ist eine Relation über $A \times B$.
    \item Für jedes $x \in A$ gibt es genau ein $y \in B$, sodass $(x,y) \in f$.
  \end{enumerate}
\end{defn}

Wir schreiben im Falle von Funktionen für $(x,y) \in f$ wie üblich auch kurz $f(x)=y$. Als Beispiel hierfür wählen wir die Nachfolgerfunktion $s: \mathbb{N} \rightarrow \mathbb{N}$:

\[ s := \{ (x,y) \in \mathbb{N} \times \mathbb{N} : S(x)=y \} \]

Es gilt ebenso wie für die Operation $S$ (welche allerdings für beliebige Mengen definiert ist) z.B.: 

\[ s(15) = 15 \cup \{ 15 \} = 16 \]

Es folgen drei weitere Axiome von $\ZFC$, welche ohne viel Erläuterung angegeben werden und welche auch im Folgenden keine Verwendung finden werden. Man kann sie also getrost fürs Erste übergehen.

\begin{axiom} [Ersetzungsaxiom]
  Für jede Menge $A$ und zu jedem Ausdruck der Gestalt $\varphi(x,y)$, der sich nicht auf $B$ bezieht, gilt:
  
  Wenn es zu jedem $x$ genau ein $y$ gibt, sodass $\varphi(x,y)$ gilt, dann gibt es eine Menge $B$, sodass es zu jedem $x \in A$ genau ein $y \in B$ mit $\varphi(x,y)$ gibt.
  
  Symbolisch: $\forall x \in A \exists! y \varphi(x,y) \rightarrow \exists B \forall x \in A \exists y \in B \varphi(x,y)$
\end{axiom}

Das Ersetzungsaxiom ist das einzige, das Fraenkel 1921 zu Zermelos Axiomen hinzugefügt hat.

\begin{axiom} [Fundierungsaxiom]
  Für jede nichtleere Menge $x$ gibt es ein $y \in x $, das keine Elemente $z$ aus $x$ enthält.
  
  Symbolisch: $\exists y (y \in x) \rightarrow \exists y (y \in x \land \lnot \exists z (z \in x \land z \in y))$
\end{axiom}

Das Fundierungsaxiom garantiert, dass Mengen nicht Mengen von sich selbst sein können (wäre nämlich $x \in x$, dann gäbe es nach dem Aussonderungsaxiom auch die Menge $y := \{ z \in x : z=x \} = \{ x \}$ und das einzige Element dieser Menge, nämlich $x$, hätte mit $y$ das Element $x$ gemeinsam). Alle Mengen würden also zur Russell-"Menge" $R$ gehören. In der üblichen Mathematik kommen tatsächlich nur solche Mengen vor. Auch dieses Axiom war in Zermelos ursprünglicher Liste der Axiome für die Mengenlehre nicht vorhanden. Es spielt auch für Beweise im Zusammenhang mit der üblichen Mathematik keine Rolle. Lediglich innerhalb der Mengenlehre werden durch das Fundierungsaxiom manche Dinge vereinfacht. So verkürzt sich der Beweis für Theorem 1 auf folgende Schritte: Wir nehmen indirekt an, eine Allmenge $V$ existiere. Dann gilt $V \in V$, da $V$ alle Mengen enthält. Dies widerspricht aber dem Fundierungsaxiom. Daher kann es keine Allmenge $V$ geben.

Zwei Mengen $x$ und $y$ heißen genau dann disjunkt, wenn $x \cap y = \emptyset$ gilt. Eine Menge $X$ besteht genau dann aus paarweise disjunkten Mengen, wenn für alle $x,y \in X$ gilt: Entweder sind $x$ und $y$ identisch, oder $x$ und $y$ sind disjunkt.

\begin{axiom} [Auswahlaxiom]
  Für alle Mengen $X$, die aus paarweise disjunkten, nichtleeren Mengen bestehen, gibt es eine Menge $Y$, welche aus jeder dieser nichtleeren Mengen genau ein Element enthält.
  
  Symbolisch: $\forall X \forall x \forall y (x \in X \land y \in Y \rightarrow x \neq \emptyset \land (x=y \lor x \cap y = \emptyset)) \rightarrow \exists Y \forall x (x \in X \rightarrow \exists z \; Y \cap x = \{z \}))$
\end{axiom}

Dieses Axiom ist zum Teil (wie oben schon bemerkt) umstritten. Dies hängt u.a. damit zusammen, dass sich aus dem Auswahlaxiom recht paradox anmutende Theoreme ableiten lassen. So haben z.B. Stefan Banach und Alfred Tarski 1924 mithilfe dieses Axioms die folgende Behauptung bewiesen: Eine Kugel kann in endlich viele Teile zerlegt werden, aus denen sich zwei Kugeln jeweils von der Größe des Originals zusammensetzen lassen. Dennoch wird das Auswahlaxiom z.B. in Analysis-Vorlesungen für erstsemestrige Mathematikstudenten und -studentinnen angewendet, meist allerdings ohne explizit darauf hinzuweisen.

\section*{Die Mengenlehre als Grundlage der Mathematik}

\begin{quote}
  Aus dem Paradies, das Cantor uns geschaffen, soll uns niemand vertreiben können. (David Hilbert, 1925)
\end{quote}

Wir haben bisher eine Art von Mengenlehre entwickelt, in der keine Urelemente vorgekommen sind, d.h. alle Elemente der jeweiligen Mengen waren selbst wiederum Mengen. Dies stellt vielleicht eine Einschränkung dar, wenn man z.B. die Menge der Mitgliedstaaten der EU angeben möchte. Wenn man die üblichen Objekte der Mathematik definieren soll, kommt man jedoch ohne Urelemente aus. Wenn man z.B. unsere Definition im letzten Abschnitt für die Menge der natürlichen Zahlen $\mathbb{N}$ studiert, wird man erkennen, dass alle ihre Elemente wiederum nur Mengen sind, welche wiederum nur Mengen enthalten usw. 

Im Folgenden möchte ich mit Mitteln der Mengenlehre die Zahlenbereiche $\mathbb{Z},\mathbb{Q}, \mathbb{R}$ und $\mathbb{C}$ entwickeln, um aufzuzeigen, dass im Grunde alle mathematischen Objekte im Rahmen der Mengenlehre zu definieren sind. Somit stellt die Mengenlehre die Grundlage für die gesamte Mathematik dar. Im Prinzip könnte man also alle mathematischen Theoreme (auch z.B. den Satz von Pythagoras oder Fermats letzten Satz) auf die Mengenlehre zurückführen und somit im Rahmen von $\ZFC$ beweisen.

Für die natürlichen Zahlen kann man die Addition und Multiplikation auf folgende Weise rekursiv definieren:

\[ x+0 := x \]
\[ x+s(y) := s(x+y) \]
\[  \]
\[ x \cdot 0 := 0 \]
\[ x \cdot s(y) := x \cdot y + x \]

Dabei greift die Definition für die Addition auf die Nachfolgerfunktion $s$ und die Definition für die Multiplikation auf die Addition $+$ zurück. Man könnte in $\ZFC$ beweisen, dass durch diese rekursiven Definitionen tatsächlich Funktionen (im Sinne des letzten Abschnittes) definiert sind und dass diese auch durch diese Definitionen eindeutig bestimmt sind. Wir werden diese Beweise jedoch an dieser Stelle übergehen.

Für diese Rechenoperationen kann man darüber hinaus in $\ZFC$ nachweisen, dass sie die üblichen Resultate liefern und dass die üblichen Rechenregeln (z.B. Assoziativität und Kommutativität) gelten. An zwei sehr einfachen Beispielen sei dies vorgeführt, dabei werden lediglich mehrmals die obigen rekursiven Definitionen angewendet:

\[ 2+2 = s(s(0)) + s(s(0)) = s( s(s(0))+s(0) ) = s(s( s(s(0))+0 )) = s(s(s(s(0))))=4 \]

\[ 1 \cdot 1 = s(0) \cdot s(0) = s(0) \cdot 0 + s(0) = 0 + s(0) = s(0+0) =s(0)=1 \]
 
Im Weiteren werden wir die folgenden Sprech- und Schreibweisen verwenden.

Seien $x,y \in \mathbb{N} \setminus \{ 0 \}$:

$x$ \emph{ist Teiler von} $y$ (in Zeichen: $x|y$) genau dann, wenn es ein $z \in \mathbb{N}$ gibt, sodass $y=z \cdot x$.

$x$ \emph{und} $y$ \emph{sind teilerfremd} (in Zeichen: $ggt(x,y)=1$) genau dann, wenn es kein $z \in \mathbb{N}$ gibt, sodass $z > 1$, $z|x$ und $z|y$. 

Kenneth Kunen (2009, 2011) gibt eine recht anschauliche Definition für die Menge der rationalen Zahlen $\mathbb{Q}$ an:

$\mathbb{Q}$ ist die Menge aller $(l,(m,n)) \in \mathbb{N} \times (\mathbb{N} \times \mathbb{N})$, für die gilt:

\begin{enumerate}
  \item $n>0$
  \item Wenn $m=0$, dann $l=0$ und $n=1$.
  \item Wenn $m \neq 0$, dann $l \in \{ 0,1 \}$ und $ggt(m,n)=1$.
\end{enumerate}

Dabei bestimmt $l$ das Vorzeichen der rationalen Zahl, d.h. ob sie nichtnegativ ($l=0$) oder negativ ($l=1$) ist. $m$ ist der Zähler und $n$ der Nenner eines \emph{vollständig gekürzten} Bruches. Da der Nenner niemals $0$ sein kann, muss $n>0$ gelten. Man überlegt sich leicht, dass auf diese Weise alle rationalen Zahlen erfasst sind:

\renewcommand{\arraystretch}{1.2}

\begin{tabular}{ll}
  $(0,(1,2))$ entspricht $\frac{1}{2}=0,5$  &  $(1,(5,4))$ entspricht $-\frac{5}{4}=-1,25$ \\
  $(0,(7,1))$ entspricht $\frac{7}{1}=7$    &  $(1,(7,1))$ entspricht $-\frac{7}{1}=-7$    \\
\end{tabular}
 
Die letzten beiden Beispiele zeigen, dass die Menge der ganzen Zahlen $\mathbb{Z}$ sich  auf folgende Weise definieren lässt (die rationale Zahl $(0,(0,1))$ entspricht $0$):

\[ \mathbb{Z} := \{ (l,(m,n)) \in \mathbb{Q} : n=1 \} \]

Wir wollen nun auch über $\mathbb{Q} \times \mathbb{Q}$ eine Kleinerrelation $<_\mathbb{Q}$ definieren.

Seien $(l',(m',n')),(l''(m'',n'')) \in \mathbb{Q}$:

$(l',(m',n')) <_\mathbb{Q} (l'',(m'',n''))$ genau dann, wenn einer der folgenden Fälle zutrifft:

\pagebreak

\begin{enumerate}
  \item $l' > l''$
  \item $l' = l''= 0$ und $m' \cdot n'' < m'' \cdot n'$ 
  \item $l' = l'' =1$ und $m' \cdot n'' > m'' \cdot n'$
\end{enumerate}

Der erste Fall besagt, dass eine rationale Zahl mit negativem Vorzeichen kleiner als eine mit nichtnegatives Vorzeichen ist. Im zweiten Fall werden zwei nichtnegative rationale Zahlen verglichen, die angegebene Ungleichung ist in diesem Fall eine Umstellung von $\frac{m'}{n'}<\frac{m''}{n''}$ (wobei hier die $<$-Relation im naiven Sinne zu verstehen ist, da diese für Brüche an dieser Stelle nicht in $\ZFC$ definiert ist). Im dritten Fall werden zwei negative rationale Zahlen verglichen. 

Im Folgenden soll klar zwischen der $<$-Relation über $\mathbb{N} \times \mathbb{N}$ und der $<_\mathbb{Q}$-Relation über $\mathbb{Q} \times \mathbb{Q}$ unterschieden werden. Des Weiteren müsste man auch auf $\mathbb{Q}$ eine Addition $+$ und Multiplikation $\cdot$ definieren, was an dieser Stelle jedoch nicht näher ausgeführt sei (bei diesen Rechenoperationen werden wir auf einen $\mathbb{Q}$-Index verzichten und diese mit der Addition und Multiplikation auf $\mathbb{N}$ "identifizieren").

Um die Menge der reellen Zahlen $\mathbb{R}$ zu definieren, greifen wir auf eine Idee von Richard Dedekind zurück. Dieser hatte in einem berühmten Text von 1872 bestimmte Teilmengen von $\mathbb{Q}$, die heute so genannten Dedekindschen Schnitte, als reelle Zahlen definiert. Ein solcher Schnitt $D$ ist eine Teilmenge von $\mathbb{Q}$, welche die folgenden Bedingungen erfüllt:

\begin{enumerate}
  \item $D \neq \emptyset$ und $D \neq \mathbb{Q}$.
  \item $D$ hat kein größtes Element bezüglich der $<_\mathbb{Q}$-Relation (d.h. wenn es auch ein solches in $\mathbb{Q}$ gibt, ist es kein Element von $D$).
  \item Für alle $p,q \in D$ gilt: Wenn $p <_\mathbb{Q} q$ und $q \in D$, dann ist auch $p \in D$.
\end{enumerate}

Wir führen drei Beispiele für Dedekindsche Schnitte an (dabei werden rationale Zahlen als Brüche angeschrieben):

$ \{ x \in \mathbb{Q} : x <_\mathbb{Q} \frac{1}{3} \}$ stellt die reelle Zahl $\frac{1}{3}$ dar.

$ \{x \in \mathbb{Q}: x \cdot x <_\mathbb{Q} 2 \} \cup \{ x \in \mathbb{Q} : x <_\mathbb{Q} 0 \}$ stellt die reelle Zahl $\sqrt{2}$ dar.

Auch die Eulersche Zahl $e$ lässt sich als Dedekindscher Schnitt darstellen: Sei für $n \in \mathbb{N}$ die folgende Folge definiert:

\[ e_n := \sum_{i=0}^n \frac{1}{i!} = 1 + \frac{1}{1} + \frac{1}{1 \cdot 2}+\frac{1}{1 \cdot 2 \cdot 3} + ... + \frac{1}{1 \cdot 2 \cdot ... \cdot n} \]

Dabei handelt es sich um die (aus der Schule bekannte) Folge von Näherungswerten an $e$. So sind alle $e_n \in \mathbb{Q}$ und die Folge ist streng monoton steigend, d.h. es gilt $e_n <_\mathbb{Q} e_{n+1}$ (für alle $n \in \mathbb{N}$). Dann stellt $\{ x \in \mathbb{Q}: \exists n \in \mathbb{N} (x <_\mathbb{Q} e_n) \}$ die Zahl $e$ dar. 

Man überlege sich, dass alle diese Beispiele die obigen Bedingungen für Dedekindsche Schnitte erfüllen. Somit kann man nun die Menge der reellen Zahlen $\mathbb{R}$ definieren:

$\mathbb{R}$ ist die Menge aller Dedekindschen Schnitte.

Auch auf dieser Menge müsste man nun mengentheoretisch eine Kleinerrelation, eine Addition und eine Multiplikation definieren (die Rechenoperationen $+$ und $\cdot$ werden wir wiederum mit den entsprechenden auf $\mathbb{Q}$ "identifizieren"). Wir übergehen jedoch, die nicht ganz leichten Definitionen hier und schreiten zur Definition des letzten Zahlenbereiches, nämlich der Menge der komplexen Zahlen $\mathbb{C}$, weiter:

$\mathbb{C} := \mathbb{R} \times \mathbb{R}$

Auf $\mathbb{C}$ legen wir auf folgende Weise eine Addition $+_\mathbb{C}$ und eine Multiplikation $\cdot_\mathbb{C}$ fest.

Seien $(x',y'),(x'',y'') \in \mathbb{C}$, dann gilt:

\[ (x',y') +_\mathbb{C} (x'',y'') = (x' + x'', y' + y'') \]
\[ (x',y') \cdot_\mathbb{C} (x'',y'') = (x' x'' - y' y'', x' y'' + x'' y') \]  

Schreibt man komplexe Zahlen $(x,y)$ wie üblich als $x + i y$ und berücksichtigt man, dass für das Quadrat der imaginären Einheit $i^2 = i \cdot i = -1$ gilt, so kann man obige Definitionen für die Addition $+_\mathbb{C}$ und die Multiplikation $\cdot_\mathbb{C}$ schnell nachvollziehen:

\[ (x'+ i y') +(x'' + i y'') = (x' +x'') + i (y' + y'') \]
\[ (x' + i y') \cdot (x'' + i y'') = x' x'' + i x' y'' + i x'' y' + i^2 y' y'' = (x' x'' - y' y'') + i (x' y'' + x'' y') \]

\vspace{2cm}
\noindent
{\Large \emph{Literatur:}}

\vspace{0.3cm}
\noindent
Ebbinghaus, Heinz-Dieter (2003): Einführung in die Mengenlehre. 4. Auflage. Heidelberg, Berlin: Spektrum Akademischer Verlag.

\noindent
Kunen, Kenneth (2009): The Foundations of Mathematics. London: College Publications.


\noindent
Kunen, Kenneth (2011): Set Theory. London: College Publications.

\end{document}