\documentclass[a4paper]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}

% \usepackage[ngerman]{babel}

\usepackage{amsmath}						%Mathematikpakete
\usepackage{amssymb}
\usepackage{amsthm}

\usepackage[]{graphicx}					%Graphikpaket



\DeclareMathOperator{\Const}{Const} 		%Festlegung von Funktionen
\DeclareMathOperator{\ZFC}{ZFC}
\DeclareMathOperator{\bij}{bijektiv}
\DeclareMathOperator{\Dom}{Dom}
\DeclareMathOperator{\Ran}{Ran}
\DeclareMathOperator{\Fund}{Fund}

\newtheorem{thm}{Theorem}				%Festlegungen von Bezeichnungen
\newtheorem{axiom}{Axiom}
\newtheorem{kor}{Korollar}
\newtheorem{defn}{Definition}

\title{Grundlagen der Mengenlehre}
\author{Michael Janny}
\date{ }

\begin{document}

\section*{Anhang: Formale Sprachen}

Wir haben im Vorfeld häufig Ausdrücke der Form $\exists x (x=x)$ verwendet. Wir wollen in diesem Anhang in aller Kürze erklären, wie diese Ausdrücke einer formalen Sprache zu lesen sind und wollen auch genauer ausführen, wie man eine solche formale Sprache Schrittweise aufbauen kann. Es sei dabei beachtet, dass wir eine solche formale Sprache nicht innerhalb von $\ZFC$ entwickeln, sondern dass wir dafür eine Metasprache verwenden. Wenn wir also im Weiteren z.B. den Begriff der "Menge" verwenden, so werden wir diesen in seiner naiven Bedeutung verstehen. 

Die logischen Zeichen unserer formalen Sprache sind die folgenden (darunter ist auch angegeben, wie sie üblicherweise gelesen werden):

\begin{tabular}{cccc}
   $\lnot$ & $\land$ & $\lor$ & $\rightarrow$  \\
   nicht & und & oder & wenn ..., dann ...  \\
  \\

  $\leftrightarrow$ & $\exists$ & $\forall$ & $=$  \\
  ... genau dann, wenn ... & es gibt & für alle & identisch mit \\
  \\
  
  $($ & $,$ & $)$ & \\
  Klammer auf & & Klammer zu & \\
  \\
\end{tabular}


Als logische Zeichen verwenden wir darüber hinaus als Variablen die Kleinbuchstaben $v,w,x,y,z$. Eventuell werden Variablen auch mit Indizes $x_1, x_2, ..., x_{10}, ...$ versehen.

Weiters wird für jede formale Sprache eine Menge $L$ ihrer nichtlogischen Zeichen festgelegt, wobei für jedes dieser Zeichen eine Stellenzahl $n$ angegeben sei. Zum einen werden wir $n$-stellige Funktionszeichen (z.B. $f,g,h$) verwenden, wobei $0$-stellige Funktionszeichen auch Konstanten genannt seien. Zum anderen werden wir $n$-stellige Prädikatzeichen (z.B. $P,Q,R$) verwenden, wobei $0$-stellige Prädikatzeichen auch Aussagen genannt seien.

Sind in der Mengenlehre noch keine Operationen wie z.B. $\cap$ oder $\cup$, und keine Konstanten wie z.B. $\emptyset$ definiert, so ist für die Sprache der Mengenlehre die Menge der nichtlogischen Zeichen $L = \{ \in \}$, wobei $\in$ ein $2$-stelliges Prädikatzeichen ist. Im Übrigen entsprechen in der Mengenlehre die oben erwähnten Operationen $\cap$ und $\cup$ den hier eingeführten Funktionszeichen und nicht den mengentheoretischen Funktionen ($f:A \rightarrow B$), wobei man dann z.B. als Menge der nichtlogischen Zeichen $L = \{ \in, \cap,\cup \}$ wählen müsste.

Die folgenden beiden Definitonen legen die Syntax unserer formalen Sprachen fest:

\begin{defn}
  Wir definieren $L$-Terme rekursiv:
  \begin{enumerate}
     \item Alle Variablen und alle Konstanten sind $L$-Terme.
     \item Ist $n>0$ und $f$ ein $n$-stelliges Funktionszeichen aus $L$ und sind $\tau_1,...,\tau_n$ $L$-Terme, dann ist auch $f(\tau_1,...,\tau_n)$ ein $L$-Term.
  \end{enumerate}
\end{defn}
   
Sind z.B. $f$ eine $2$-stelliges, $g$ ein $1$-stelliges Funktionszeichen und $a$ eine Konstante, dann sind folgende Zeichenketten $\{ f,g,a \}$-Terme:

\begin{tabular}{llllll}
  $f(x,y)$ & $g(x)$ & $g(a)$ & $f(g(x),g(y))$ & $g(g(x))$ & $g(g(g(g(a))))$ \\
\end{tabular}

\begin{defn}
  Wir definieren $L$-Formeln rekursiv:
  \begin{enumerate}
    \item Sind $\tau_1$ und $\tau_2$ $L$-Terme, so ist $(\tau_1=\tau_2)$ eine $L$-Formel.
    \item Ist $P$ ein $n$-stelliges Prädikatzeichen aus $L$ und sind $\tau_1,...,\tau_n$ $L$-Terme, dann ist $P(\tau_1,...,\tau_n)$ eine $L$-Formel.
    \item Sind $\varphi$ und $\psi$ $L$-Formeln, dann sind auch $\lnot \varphi$, $(\varphi \land \psi)$, $(\varphi \lor \psi)$, $(\varphi \rightarrow \psi)$ und $(\varphi \leftrightarrow \psi)$ $L$-Formeln.
    \item Ist $\varphi$ eine $L$-Formel und $x$ eine beliebige Variable, dann sind auch $\exists x \varphi$ und $\forall x \varphi$ $L$-Formeln.
  \end{enumerate}
\end{defn}
 
Ist z.B. $P$ ein $2$-stelliges Prädikatzeichen und $f$ ein $1$-stelliges Funktionszeichen, dann sind folgende Zeichenketten $\{ P,f \}$-Formeln:

\begin{tabular}{lll}
  $\lnot P(f(x),y)$ & $(P(x,f(y)) \land (x=y))$ & $\exists z (P(x,z) \rightarrow P(z,x))$ \\ 
\end{tabular}

Für die Mengenlehre geben wir ebenfalls zwei Beispiele für $\{ \in \}$-Formeln an:

\begin{tabular}{ll}
  $(\forall z (z \in x \leftrightarrow z \in y) \rightarrow x=y)$ & $\exists z (x \in z \land y \in z)$ \\
\end{tabular}

Im Haupttext und auch im Folgenden werden wir auf Klammern verzichten, wenn dies zu keinen Missverständnissen führt. Darüber hinaus erlauben wir uns auch andere Buchstaben als die angegebenen als Variablen zu verwenden. Die folgenden Schreibweisen sollen als Abkürzungen der angegebenen Formeln bzw. $\{ \in \}$-Formeln gelten:

$(x \neq y)$ steht für $\lnot (x = y)$.

$\exists x \in z \; \varphi$ steht für $\exists x (x \in z \rightarrow \varphi)$.
  
$\forall x \in z \; \varphi$ steht für $\forall x (x \in z \land \varphi$).
  
$\exists! x \; \varphi$ steht für $\exists x ( \varphi \land \forall y (\varphi[y] \rightarrow x=y))$.
  
In letzterer Formel steht $\varphi[y]$ für die Formel, die aus $\varphi$ entsteht, wenn alle $x$ in ihr, welche nicht im Wirkungsbereich eines Quantors $\exists$ oder $\forall$ stehen, durch ein $y$ ersetzt werden.

Wir führen in den folgenden Definitionen für unsere formalen Sprachen eine Semantik ein, welche festlegt, wie die $L$-Terme und $L$-Formeln zu interpretieren sind. Hier steht $A^n$ für ein $n$-faches kartesisches Produkt, d.h. beispielsweise $A^3 = A \times (A \times A)) = A \times A \times A$.  

\begin{defn}
  Sei $L$ eine Menge von nichtlogischen Zeichen. Eine $L$-Struktur ist ein Paar $\mathfrak{A}=(A,I)$, wobei $A$ eine nichtleere Menge und $I$ eine Funktion ist, welche jedem Element $s$ aus $L$ auf folgende Weise Entitäten zuordnet (wir schreiben für $I(s)$ im Weiteren auch $s_\mathfrak{A}$):
  
  \begin{itemize}
    \item Ist $n>0$ und $f$ ein $n$-stelliges Funktionszeichen, dann ist $I(f) = f_\mathfrak{A}: A^n \rightarrow A$.
    \item Ist $n>0$ und $P$ ein $n$-stelliges Prädikatzeichen, dann ist $I(P) = P_\mathfrak{A} \subseteq A^n$.
    \item Ist $c$ eine Konstante, dann ist $I(c) = c_\mathfrak{A} \in A$.
    \item Ist $\varphi$ eine Aussage, dann ist $I(\varphi) = \varphi_\mathfrak{A} \in \{ W,F \}$.
  \end{itemize}
\end{defn}

Dabei stehen $W$ und $F$ für die Wahrheitswerte, d.h. für "wahr" und "falsch".

Wir legen z.B. auf der Menge der natürlichen Zahlen $\mathbb{N}$ eine $\{ +,\cdot \}$-Struktur $\mathfrak{N}$ fest. $I(+) = +_\mathfrak{N}$ und $I(\cdot) = \cdot_\mathfrak{N}$ sind dann Funktionen von $\mathbb{N} \times \mathbb{N}$ nach $\mathbb{N}$, welche der üblichen Addition und Multiplikation über $\mathbb{N}$ entsprechen sollen.

Auch für die Mengenlehre legen wir auf der naiven (!) Menge des Mengenuniversums $U$ eine $ \{ \in \}$-Struktur $\mathfrak{U}$ fest. Hier könnte $I(\in) = \in_\mathfrak{U}$ mit der üblichen (naiven) Elementbeziehung auf $U$ zusammenfallen. (Aufgrund von Theorem 1 aus dem Hauptteil kann natürlich $U$ kein Element von sich selbst sein.) 

Die folgende Definition legt jeweils fest, wie die Variablen unserer formalen Sprache mit Elementen aus der Menge $A$ belegt werden.

\begin{defn}
  Sei $\mathfrak{A}:=(A,I)$ eine $L$-Struktur. Dann verstehen wir unter einer Belegung $\sigma$ eine Funktion, die jeder Variable unserer formalen Sprache ein Element von $A$ zuordnet. 
\end{defn}

In der nächsten Definition wird festgelegt, wie bei bekannter $L$-Struktur $\mathfrak{A}$ und Belegung $\sigma$ $L$-Terme interpretiert werden.

\begin{defn}
  Seien $\mathfrak{A} = (A,I)$ eine $L$-Struktur und $\sigma$ eine entsprechende Belegung. Dann setzen wir die Funktion $I$ auf $L$-Terme $\tau$ auf folgende Weise fort (wobei wir in eckigen Klammern die verwendete Belegung angeben und für $I(\tau)[\sigma]$ die Schreibweise $\tau_\mathfrak{A}[\sigma]$ verwenden):
  \begin{itemize}
    \item $I(x)[\sigma] = x_\mathfrak{A}[\sigma] = \sigma(x)$, falls $x$ eine Variable ist.
    \item $I(c)[\sigma] = c_\mathfrak{A}[\sigma] = c_\mathfrak{A}$, falls $c$ eine Konstante aus $L$ ist.
    \item $I(f(\tau_1,...,\tau_n))[\sigma] = f(\tau_1,...,\tau_n)_\mathfrak{A}[\sigma] = f_\mathfrak{A}({\tau_1}_\mathfrak{A}[\sigma],...,{\tau_n}_\mathfrak{A}[\sigma])$, falls $n>0$, $f$ ein $n$-stelliges Funktionszeichen aus $L$ und $\tau_1,...,\tau_n$ $L$-Terme sind.
  \end{itemize}   
\end{defn}

Belegt man in der obigen $\{ +, \cdot \}$-Struktur $\mathfrak{N}$ für die natürlichen Zahlen die Variablen $x$ und $y$ mit: $\sigma(x)=2$ und $\sigma(y)=3$. Dann gilt z.B., wenn wir wie üblich $(x+y)$ statt $+(x,y)$ schreiben:

\[ (x + y)_\mathfrak{N}[\sigma] = +(x,y)_\mathfrak{N}[\sigma] = +_\mathfrak{N}(\sigma(x),\sigma(y)) = +_\mathfrak{N}(2,3) = 5 \]

Nun können wir auch $L$-Formeln für $L$-Strukturen $\mathfrak{A}$ und ihre Belegungen $\sigma$ interpretieren. Dabei verwenden wir die Schreibweise $\sigma+(x/a)$ für diejenige Belegung, welche mit $\sigma$ in allen Variablen außer $x$ übereinstimmt und $x$ der Wert $a \in A$ zugeordnet wird.

\begin{defn}
   Seien $\mathfrak{A} = (A,I)$ eine $L$-Struktur. Dann ist $\mathfrak{A}$ Modell für eine $L$-Formel $\varphi$ und eine entsprechende Belegung $\sigma$ (in Zeichen: $\mathfrak{A} \models \varphi [\sigma]$), wenn sich dies aus folgenden Bedingungen ergibt:
   
   \begin{itemize}
     \item $\mathfrak{A} \models \varphi [\sigma]$, falls $\varphi$ eine Aussage aus $L$ ist und $\varphi_\mathfrak{A} = W$.
     \item $\mathfrak{A} \models (\tau_1 = \tau_2) [\sigma]$, falls $\tau_1$ und $\tau_2$ $L$-Terme sind und ${\tau_1}_\mathfrak{A}[\sigma] = {\tau_2}_\mathfrak{A}[\sigma]$.
     \item $\mathfrak{A} \models P(\tau_1,...,\tau_n) [\sigma]$, falls $n>0$, $P$ ein $n$-stelliges Prädikatzeichen aus $L$, $\tau_1,...,\tau_n$ $L$-Terme sind und $({\tau_1}_\mathfrak{A}[\sigma],...,{\tau_n}_\mathfrak{A}[\sigma])$ ein Element von $P_\mathfrak{A}$ ist.
     \item $\mathfrak{A} \models \lnot \varphi [\sigma]$, falls $\varphi$ $L$-Formel ist und nicht $\mathfrak{A} \models \varphi [\sigma]$ gilt.
     \item $\mathfrak{A} \models (\varphi \land \psi) [\sigma]$, falls $\varphi$ und $\psi$ $L$-Formeln sind und $\mathfrak{A} \models \varphi [\sigma]$ und $\mathfrak{A} \models \psi [\sigma]$ gilt.
     \item $\mathfrak{A} \models (\varphi \lor \psi) [\sigma]$, falls $\varphi$ und $\psi$ $L$-Formeln sind und $\mathfrak{A} \models \varphi [\sigma]$ oder $\mathfrak{A} \models \psi [\sigma]$ gilt.
     \item $\mathfrak{A} \models (\varphi \rightarrow \psi) [\sigma]$, falls $\varphi$ und $\psi$ $L$-Formeln sind und nicht $\mathfrak{A} \models \varphi [\sigma]$ oder $\mathfrak{A} \models \psi [\sigma]$ gilt.
     \item $\mathfrak{A} \models (\varphi \leftrightarrow \psi) [\sigma]$, falls $\varphi$ und $\psi$ $L$-Formeln sind und $\mathfrak{A} \models \varphi [\sigma]$ genau dann, wenn $\mathfrak{A} \models \psi [\sigma]$ gilt.
     \item $\mathfrak{A} \models \exists x \varphi [\sigma]$, falls $x$ eine Variable und $\varphi$ eine $L$-Formel sind und für ein $a \in A$ gilt: $\mathfrak{A} \models \varphi [\sigma+(x/a)]$. 
     \item $\mathfrak{A} \models \forall x \varphi [\sigma]$, falls $x$ eine Variable und $\varphi$ eine $L$-Formel sind und für alle $a \in A$ gilt: $\mathfrak{A} \models \varphi [\sigma+(x/a)]$.
   \end{itemize} 
   Sind alle Variablen in einer $L$-Formel $\varphi$ durch Quantoren $\exists$ oder $\forall$ gebunden, dann nennen wir $\varphi$ auch $L$-Satz. In diesem Fall schreiben wir statt $\mathfrak{A} \models \varphi [\sigma]$ auch kurz $\mathfrak{A} \models \varphi$, da hier keine Abhängigkeit mehr von der jeweiligen Belegung $\sigma$ besteht.
\end{defn} 

Die oben eingeführte $\{ +, \cdot \}$-Struktur $\mathfrak{N}$ müsste also Modell des Kommutativgesetzes für die Addition sein, d.h.

\[ \mathfrak{N} \models \forall x \forall y (x+y = y+x) \]

Wir werden auch sagen, das Kommutativgesetz oder der angegebene $L$-Satz sei in der $\{ +, \cdot \}$-Struktur $\mathfrak{N}$ wahr.


In der folgenden Definition wird der Modellbegriff aus der letzten Definition auf $L$-Satzmengen erweitert und ein semantischer Folgerungsbegriff eingeführt.

\begin{defn}
  Sei $\mathfrak{A}$ eine $L$-Struktur und $\Sigma$ eine Menge von $L$-Sätzen.
  
  Dann ist $\mathfrak{A}$ genau dann ein Modell von der $L$-Satzmenge $\Sigma$ (in Zeichen: $\mathfrak{A} \models \Sigma$), wenn für alle $L$-Sätze $\varphi$ aus $\Sigma$ gilt: $\mathfrak{A} \models \varphi$.
  
  Sei weiters $\psi$ ein $L$-Satz:
  
  Dann ist $\psi$ genau dann eine Folgerung aus der $L$-Satzmenge $\Sigma$ (in Zeichen: $\Sigma \models \\psi$), wenn $\mathfrak{A} \models \psi$ für alle $L$-Strukturen $\mathfrak{A}$ gilt, für welche auch $\mathfrak{A} \models \Sigma$ zutrifft.
\end{defn}

D.h. der $L$-Satz $\psi$ folgt aus der $L$-Satzmenge $\Sigma$, falls er in jeder $L$-Struktur $\mathfrak{A}$ wahr ist, welche Modell von $\Sigma$ ist.

Man könnte z.B. als $\{ \in \}$-Satzmenge $\Sigma = \forall \forall \ZFC$ wählen. In dieser sollen alle Axiome von $\ZFC$ (in ihrer symbolischen Form) enthalten sein, wobei für jede nicht gebundene Variable $x$ in den Axiomen der Ausdruck $\forall x$ vorangestellt sei. So wird z.B. das Extensionalitätsaxiom in der Form $\forall x \forall y (\forall z (z \in x \leftrightarrow z \in y) \rightarrow x=y)$ in die Menge $\forall \forall \ZFC$ aufgenommen. Es sollten weiters in $\forall \forall \ZFC$ auch alle abkürzenden Schreibweisen wie z.B. $x \subseteq y$ durch äquivalente Ausdrücke, welche nur $\in$ als nichtlogisches Zeichen verwenden, ersetzt werden. Für das Aussonderungs- bzw. Ersetzungsaxiom ist darüber hinaus zu beachten, dass für jede $\{ \in \}$-Formel $\varphi(x)$ bzw. $\varphi(x,y)$ ein eigener $\{ \in \}$-Satz in die $\{ \in \}$-Satzmenge $\forall \forall \ZFC$ aufgenommen werden müsste (d.h. $\forall \forall \ZFC$ besteht aus unendlich vielen $\{ \in \}$-Sätzen).

Im Hauptteil haben wir gesehen, dass die Existenz der leeren Menge, aus den Axiomen von $\ZFC$ folgt. Die Behauptung, dass es die (bzw. eine) leere Menge gibt, könnte man mit dem folgenden $\{ \in \}$-Satz ausdrücken:

\[ \exists y \forall x \lnot (x \in y) \]

Dementsprechend müsste auch gelten:

\[ \forall \forall \ZFC \models \exists y \forall x \lnot (x \in y) \]

Das bedeutet: Jede $\{ \in \}$-Struktur, die Modell von $\forall \forall \ZFC$ ist, ist auch Modell von $\exists y \forall x \lnot (x \in y)$. Damit kann man im Prinzip aus $\forall \forall \ZFC$ alles folgern, was wir im Hauptteil abgeleitet haben. Üblicherweise entwickelt man für formale Sprachen auch einen rein syntaktischen Ableitungsbegriff, in welchem man auf keine $L$-Strukturen zurückgreift, darauf wollen wir hier jedoch nicht näher eingehen. Näheres darüber findet man in jedem Buch über mathematische Logik.


\end{document}